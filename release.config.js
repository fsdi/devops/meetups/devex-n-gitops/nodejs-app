/* eslint-disable no-template-curly-in-string */

const gitlabUrl = 'https://gitlab.com'
const gitlabApiPathPrefix = '/api/v4'
const assets = [
  { path: 'api.js', label: 'api distribution' }
]

const verifyConditions = [
  [
    '@semantic-release/git',
  ],
  [
    '@semantic-release/gitlab',
    {
      gitlabUrl,
      gitlabApiPathPrefix,
      assets
    }
  ]
]
const analyzeCommits = [[
  '@semantic-release/commit-analyzer',
]]

const generateNotes = ['@semantic-release/release-notes-generator']
const prepare = [
  // '@semantic-release/changelog',
  [
    '@semantic-release/git',
    {
      assets: ['package.json'],
      message: 'chore(release): ${nextRelease.version} [skip ci]\n\n${nextRelease.notes}'
    }
  ]
]
const publish = [[
  '@semantic-release/gitlab',
  {
    gitlabUrl,
    gitlabApiPathPrefix,
    assets
  }
]]

// skipped steps
const verifyRelease = []
const fail = []
const success = []
const addChannel = []

module.exports = {
  repositoryUrl: 'git@gitlab.com:fsdi/devops/meetups/obca/nodejs-app.git',
  branches: ['main'],
  verifyConditions,
  analyzeCommits,
  verifyRelease,
  generateNotes,
  prepare,
  publish,
  fail,
  success,
  addChannel
}