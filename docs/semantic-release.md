


```js
/* eslint-disable no-template-curly-in-string */

const gitlabUrl = 'https://gitlab.com'
const gitlabApiPathPrefix = '/api/v4'
const assets = [
  { path: 'index.js', label: 'JS distribution' }
]

const verifyConditions = [
  ['@semantic-release/changelog'],
  [
    '@semantic-release/git',
    {
      assets: ['package.json', 'CHANGELOG.md'],
      message: 'chore(release): ${nextRelease.version} [skip ci]\n\n${nextRelease.notes}'
    }
  ],
  [
    '@semantic-release/gitlab',
    {
      gitlabUrl,
      gitlabApiPathPrefix,
      assets
    }
  ]
]
const analyzeCommits = [[
  '@semantic-release/commit-analyzer',
  {
    preset: 'angular',
    releaseRules: './config/cz-coventional-changelog-custom/rules.js'
  }
]]

const generateNotes = ['@semantic-release/release-notes-generator']
const prepare = [
  '@semantic-release/changelog',
  ["@semantic-release/npm", { // this here is just for package.log update
    "npmPublish": false,
  }],
  [
    '@semantic-release/git',
    {
      assets: ['package.json', 'CHANGELOG.md'],
      message: 'chore(release): ${nextRelease.version} [skip ci]\n\n${nextRelease.notes}'
    }
  ]
]
const publish = [[
  '@semantic-release/gitlab',
  {
    gitlabUrl,
    gitlabApiPathPrefix,
    assets
  }
]]

// skipped steps
const verifyRelease = []
const fail = []
const success = []
const addChannel = []

module.exports = {
  repositoryUrl: 'git@gitlab.com:fsdi/devops/meetups/obca/nodejs-app.git',
  branches: ['master'],
  verifyConditions,
  analyzeCommits,
  verifyRelease,
  generateNotes,
  prepare,
  publish,
  fail,
  success,
  addChannel
}
```

# Ci with `Gitlab`

```yaml
# gitlab-ci.yml

stages:
  - stage:release

# This version is important! See https://github.com/semantic-release/gitlab/issues/139
image: node:12.14.1

# This folder is cached between builds
# http://docs.gitlab.com/ce/ci/yaml/README.html#cache
cache:
  paths:
    - node_modules/

# This selects only refs matching master
semantic_release: &only_semantic_release
  only:
    - master

step:release:semantic_release:
  extends: semantic_release
  when: manual
  stage: stage:release
  before_script:
    - git config --global user.name "${GITLAB_USER_NAME}"
    - git config --global user.email "${GITLAB_USER_EMAIL}"
    - echo "$GITLAB_USER_EMAIL"
    - echo "$GITLAB_USER_NAME"
    - echo "$CI_GIT_TOKEN"
  script:
    - npm i --non-interactive --pure-lockfile
    - npm run semantic-release

```
