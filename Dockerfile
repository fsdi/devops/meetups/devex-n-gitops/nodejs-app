FROM node:14.2-alpine

RUN mkdir -p /app/config

WORKDIR /app/

COPY package.json .
RUN npm install

COPY *.js /app/
COPY config/* /app/config/

EXPOSE 8080

CMD [ "npm", "start" ]
