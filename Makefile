#!/usr/bin/make -f

# DOCKER_USERNAME:= hagzag
# REPO:= docker.io
# REGISTRY := $(REPO)/$(DOCKER_USERNAME)


DOCKER_BIN 		 := /usr/local/bin/docker
REDIS_PASSWORD := 'MyS3cr3t'

RELEASE_NAME	 := ping-app
CHART_BASE_DIR := ./deployment/helm/charts
CHART_NAME		 := pingalicious

.PHONY: docker-build docker-push docker-stop docker-clean-run docker-run

# node-* run nodejs local 
docker-check:
	@docker ps &>/dev/null || echo "docker not running"

redis-container-start: docker-check
	@docker ps | grep redis || \
	 docker run -d --name redis -p 6379:6379 redis redis-server --requirepass $(REDIS_PASSWORD)  && \
	 docker ps | grep redis

redis-container-stop: docker-check
	@docker stop redis

redis-container-cleanup:
	@docker rm -f redis

# node-* run nodejs local 

node-cleanup:
	@rm -rf ./node_modules

node-yarn-install:
	@npx yarn

node-run-local: node-yarn-install redis-container-start
	@npx yarn start
